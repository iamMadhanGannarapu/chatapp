import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { user } from '../modal/users'
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  userSession: string;
  initSession() {
    this.userSession = sessionStorage.getItem('userData')
  }
  url = "http://localhost:3400";
  constructor(private http: HttpClient) { }
  register(data: user): Observable<any> {
    alert(JSON.stringify(data))
    return this.http.post(this.url + "/register", JSON.stringify(data), this.httpOptions)
  }
  login(data: user): Observable<any> {
    alert(JSON.stringify(data))
    return this.http.post(this.url + "/login", JSON.stringify(data), this.httpOptions)
  }
  sessionData(): Observable<any> {
    this.initSession();
    return this.http.get(this.url + "/sessionData/" + this.userSession, this.httpOptions)
  }
  registeredUsers(): Observable<any> {
    return this.http.get(this.url + "/users", { responseType: "json" })
  }

  connect(frmid: any, toid: any): Observable<any> {
    alert(frmid + "-" + toid)
    return this.http.post(this.url + "/connect", { fromid: frmid, toid: toid }, this.httpOptions)
  }
  getUserConnections(id: any): Observable<any> {
    return this.http.get(this.url + "/myConnections/" + id, { responseType: 'json' })
  }
  acceptRequest(frmid: any, toid: any): Observable<any> {
    return this.http.post(this.url + "/acceptConnection", { fromid: frmid, toid: toid }, this.httpOptions)
  }
}
