import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private socket = io('http://localhost:8080');
  constructor() { }
  sendMessage(message) {
    this.socket.emit('new-message', message);
  }
}
