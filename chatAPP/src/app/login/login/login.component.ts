import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { user } from '../../modal/users';
import { LoginService } from '../../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userObj: user;
  loginFailed: string;
  checkData: any;
  constructor(private router: Router, private ls: LoginService) {
    this.userObj = new user();

  }

  ngOnInit() {
  }
  sendData() {
    // alert(JSON.stringify(this.userObj))
    this.ls.login(this.userObj).subscribe((data) => {
      sessionStorage.setItem("userData", data.data[0].usersession)
      if (data.data == "NDF") {
        this.loginFailed = "";
      } else {
        this.router.navigate(['chat/one-one']);
        // this.checkData = data.data
      }
    })
  }
  redirect() {
    this.router.navigate(['login/signup']);
  }
}
