import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { user } from '../../modal/users'
import { LoginService } from '../../service/login.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  userObj: user;

  checkData: string;
  constructor(private router: Router, private ls: LoginService) {
    this.userObj = new user();
  }

  ngOnInit() {
  }
  register() {
    if (this.userObj.password == this.userObj.confirmpassword && this.userObj.password != "" && this.userObj.password != null) {
      // var data = [];
      // data.push({ userId: this.userObj.id, userName: this.userObj.username, password: this.userObj.password })
      // localStorage.setItem("usersData", JSON.stringify(data));

      this.ls.register(this.userObj).subscribe((data) => {
        alert("registration done sucessfully");
        this.router.navigate(['chat/one-one']);

      })

    } else {
      alert("please fill the details to register")
    }
  }

}
