import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ChatRoutingModule } from './chat-routing.module';
import { OneToOneChatComponent } from './one-to-one-chat/one-to-one-chat.component';
import { GroupChatComponent } from './group-chat/group-chat.component';

@NgModule({
  declarations: [OneToOneChatComponent, GroupChatComponent],
  imports: [
    CommonModule,
    ChatRoutingModule,
    HttpClientModule,
    FormsModule
    
  ]
})
export class ChatModule { }
