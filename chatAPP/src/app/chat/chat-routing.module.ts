import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { OneToOneChatComponent } from './one-to-one-chat/one-to-one-chat.component';

const routes: Routes = [
  { path: "groupChat", component: GroupChatComponent },
  { path: "one-one", component: OneToOneChatComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
