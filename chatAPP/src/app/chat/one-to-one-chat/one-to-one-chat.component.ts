import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../service/chat.service';
import { LoginService } from '../../service/login.service';

@Component({
  selector: 'app-one-to-one-chat',
  templateUrl: './one-to-one-chat.component.html',
  styleUrls: ['./one-to-one-chat.component.scss']
})
export class OneToOneChatComponent implements OnInit {
  message: string;
  messageArray = []
  users: any;
  sessiondata: any;
  userid: any;
  myConn: any;
  constructor(private chatService: ChatService, private ls: LoginService) { }

  ngOnInit() {
    this.ls.sessionData().subscribe((data) => {
      this.sessiondata = data;
      console.log("session data");
      console.log(data);
      this.userid = data[0].id;
      this.myConnections();

    });
    this.ls.registeredUsers().subscribe((data) => {
      this.users = data
      console.log('users data');
      console.log(data);
    });
  };
  connect(toid) {
    this.ls.connect(this.userid, toid).subscribe(() => {
      alert("request sent");
    })
  }
  myConnections() {
    alert(this.userid)
    this.ls.getUserConnections(this.userid).subscribe((data) => {
      this.myConn = data;
      console.log("my connections");

      console.log(data);

    })
  }

  sendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = '';
  }
}
