import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './login/home/home.component';

const routes: Routes = [
  { path: 'chat', loadChildren: '../app/chat/chat.module#ChatModule' },
  { path: 'login', loadChildren: '../app/login/login.module#LoginModule' },
  // { path: "", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
