import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'chatAPP';
  checkData: string;
  constructor(private router: Router) { }
  ngOnInit() {
    this.checkData = sessionStorage.getItem("userData");
    console.log(this.checkData);
  }
  logout() {
    sessionStorage.removeItem('userData');
    this.checkData = ""
    this.router.navigate(['login/login']);
  }
}
