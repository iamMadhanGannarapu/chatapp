const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', function (req, res) {
    res.send('Hello world!');
});

io.sockets.on('connection', function (socket) {
    console.log("socket connection made");
    socket.on('new-message', (message) => {
        console.log(message);
    })
});

const server = http.listen(8080, function () {
    console.log('http://localhost:8080');
});