var express = require('express');
var promise = require('bluebird');
var bodyparser = require('body-parser');
var get_ip = require('ipware')().get_ip;

var path = require('path');
var options = {
    promiseLib: promise,
    query(e) {
        console.log('QUERY:', e.query);
    }
};
var pgp = require('pg-promise')(options);
var app = new express();
var connectionString = 'postgres://postgres:root1@localhost:5432/chatdatabase';
var db = pgp(connectionString);
app.use(express.static(path.resolve(__dirname, "public")));
app.use(bodyparser.json({
    limit: '10mb',
    extended: true
}));
app.use(bodyparser.urlencoded({
    limit: '6mb',
    extended: true
}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
// ------------------------------
app.get('/', (req, res, next) => {
    res.send('connected sucessfully')
})
app.post('/register', (req, res, next) => {

    var firstname = req.body.username;
    var mobile_no = req.body.mobile;
    var email_id = req.body.mail;
    var password = req.body.password;
    db.any('select * from fn_adduserregistration($1,$2,$3)', [firstname, mobile_no, email_id]).then((data) => {
        var userid = data[0].fn_adduserregistration;
        db.any('select * from fn_userloginsession($1)', [email_id]).then((sess) => {
            db.any('select * from fn_login($1,$2,$3)', [userid, email_id, password]).then((login) => {
                db.any('select * from loginview where emailid=$1', [email_id]).then((login) => {
                    console.log(login);
                    var pwd = login[0].password;
                    var name1 = login[0].name;
                    var epwd = name1.substring(3, 5) + pwd + email_id.slice(0, 3)
                    db.any('update login set password=$1 where emailid=$2', [epwd, email_id]).then((upd) => {
                        db.any('insert into authentication (userid,emailid,password,sucessfullogins,loginfreq) values($1,$2,$3,0,0)', [userid, email_id, epwd]).then((auth) => {
                            res.send(auth)
                        })
                    })
                })
            })
        })
    })
});
app.post('/login', function (req, res, next) {
    console.log(req.body);

    var emailid = req.body.mail;
    var pwd = req.body.password;
    var property = 'true'
    var dtype = "req.device.type.toUpperCase()"
    var ip_info = get_ip(req);
    var ip = ip_info.clientIp.substring(7, 17);
    db.any('select * from loginview where emailid=$1', emailid).then((emailidCheck) => {
        if (emailidCheck.length == 1) {
            var name1 = emailidCheck[0].name;
            console.log(emailid, pwd);
            console.log(name1.substring(3, 5));
            console.log(pwd);
            console.log(emailid.slice(0, 3));
            var epwd = name1.substring(3, 5) + pwd + emailid.slice(0, 3);
            console.log(emailid, epwd);

            db.any('select * from login where emailid=$1 and password=$2', [emailid, epwd]).then((data) => {
                if (data.length > 0) {
                    console.log(data);

                    var userid = data[0].registrationid
                    db.any('update authentication set lastaccessed=current_timestamp,loginsfailed=0,devicetype=$1,acessedip=$2,sucessfullogins=((select sucessfullogins from authentication where userid=$4)+1),loginfreq=((select loginfreq from authentication where userid=$5)+1) where userid=$3', [dtype, ip, userid, userid, userid]).then((authe) => {
                        var lid = data[0].emailid;
                        db.any('select * from fn_userloginsession($1)', [lid]).then((sess) => {
                            var s1 = sess[0].fn_userloginsession
                            db.any('select * from viewallregistration where usersession=$1', s1).then((session) => {
                                db.any('select status from registration where emailid=$1', lid).then((status) => {
                                    if (data[0].firstlogin == false) {
                                        db.any('update login set firstlogin=$2 where userid=$1', [userid, property]).then((updates) => {
                                            db.any('select * from login where emailid=$1', lid).then((flogin) => {
                                                var stats = flogin[0].firstlogin;
                                                if (stats == true) {
                                                    db.any('update authentication set initiallogin=current_timestamp where userid=$1', flogin[0].userid).then(() => {

                                                    })
                                                }
                                            })
                                        })
                                    }
                                    console.log(session);

                                    if (session) {
                                        res.status(200).send({
                                            result: true,
                                            error: "NOERROR",
                                            data: session,
                                            message: "Login Successful"
                                        })
                                    }
                                })
                            })
                        })
                    })
                    data[0].emailid = emailid;
                }
            })
        } else {
            res.status(200).send({
                result: true,
                error: "NOERROR",
                data: "NDF",
                message: "Login Failed!"
            })
        }
    })
})
app.get("/sessionData/:session", (req, res, next) => {
    var session = req.params.session
    db.any("select * from fn_viewuserid($1)", [session]).then((data) => {
        res.send(data)
        // console.log(data);

    })
})
app.get("/users", (req, res, next) => {
    db.any("select * from registrationview").then((data) => {
        res.send(data)
        // console.log(data);

    })
})
app.post("/connect", (req, res, next) => {
    var fromId = req.body.fromid;
    var toId = req.body.toid;
    var status = "req sent"
    db.any("insert into connections (senderid,receiverid,status) values($1,$2,$3)", [fromId, toId, status]).then((data) => {
        res.send(data);
    })
});
app.get("/myConnections/:id", (req, res, next) => {
    var id = req.params.id;
    db.any("select * from connections where status=$1 and receiverid=$2", ["req sent", id]).then((data) => {
        res.send(data)
        console.log(data);
        
    })
})
app.post("/acceptConnection", (req, res, next) => {
    var fromId = req.body.fromid;
    var toId = req.body.toid;
    var status = "accepted"
    db.any("update connection set status=$1 where receiverid=$2,senderid=$3", [fromId, toId, status]).then((data) => {
        res.send(data);
    })
})


// ----------------------------------
app.listen(3400, (err) => {
    if (err) {
        console.log('Unable to start server ...')
    } else {
        console.log('server Started at : ' + 'http://localhost:3400')
    }
})