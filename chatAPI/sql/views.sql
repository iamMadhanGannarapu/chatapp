
CREATE OR REPLACE VIEW public.loginview AS
 SELECT l.registrationid,
    l.emailid,
    l.password,
    r.name,
    r.emailid AS mail,
    r.mobile
   FROM login l
     JOIN registration r ON l.registrationid = r.id;

CREATE OR REPLACE VIEW public.registrationview AS
 SELECT ur.id,
    ur.name,
    ur.mobile,
    ur.emailid,
    ur.status,
    ul.usersession
   FROM registration ur
     JOIN userlogin ul ON ur.id = ul.userid;


     CREATE OR REPLACE VIEW public.viewallregistration AS
 SELECT ul.usersession,
    reg.id,
    reg.name,
    reg.mobile,
    reg.emailid
   FROM userlogin ul
     JOIN registration reg ON reg.id = ul.userid
     JOIN login lg ON lg.registrationid = reg.id;


