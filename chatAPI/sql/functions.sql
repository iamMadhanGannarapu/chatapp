CREATE OR REPLACE FUNCTION public.fn_adduserregistration(
	para_name character varying,
	param_mobile character varying,
	para_emailid character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
    declare n int;
    begin
	Insert into registration(name,mobile,emailid)
	Values(para_name,param_mobile,para_emailid)
	returning  id into  n;
	return n;
	end
    $BODY$;



CREATE OR REPLACE FUNCTION public.fn_login(
	param_userid integer,
	param_emailid character varying,
	param_pass character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare a varchar;
begin
insert into login(registrationid,emailid,password)values(param_userid,param_emailid,param_pass) returning id into a;
return a;
end
$BODY$;

CREATE OR REPLACE FUNCTION public.fn_userloginsession(
	param_emailid character varying)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare n text;
declare m text;
declare x int;
begin
select id from registration where emailid=param_emailid into x;
SELECT md5(random()::text || clock_timestamp()::text)::uuid into n;
insert into userlogin(userid,emailid,usersession)values(x,param_emailid,n) on conflict(emailid) do update set usersession=n returning usersession into m;
return m;
end
$BODY$;



CREATE OR REPLACE FUNCTION public.fn_viewuserid(
	param_usersession text)
    RETURNS SETOF registrationview 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
begin
return query
select * from registrationview 
where id= (select u.userid from userlogin u where u.usersession= param_usersession);
end;
$BODY$;


CREATE OR REPLACE FUNCTION public.login(
	param_userid integer,
	param_emailid character varying,
	param_pass character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare a varchar;
begin
insert into login(userid,emailid,password)values(param_userid,param_emailid,param_pass) returning id into a;
return a;
end
$BODY$;
