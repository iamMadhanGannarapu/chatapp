create table registration(
	id serial primary key,
	first_name character varying not null,
	last_name character varying not null,
	mobile_no character varying  NOT NULL,
	email_id character varying  NOT NULL unique
);
create table userLogin(
	id serial primary key,
	userid int references registration(id),
	emailid varchar unique,
	usersession text unique
);
create table login (
	id serial primary key,
	userid int references registration(id),
	emailid varchar,
	password varchar
);

create table authentication(
    id serial primary key,
    userid int references registration(id),
    emailid varchar,
    password varchar,
    lastaccessed timestamp,
    lastmodified timestamp,
    sucessfullogins int,
    loginsfailed int,
    loginfreq int,
    lastunsuccessfulaccess timestamp,
    initiallogin timestamp,
    devicetype varchar,
    acessedip text
);
create table connections(
    id serial primary key,
    senderid int,
    receiverid int,
    status varchar);


